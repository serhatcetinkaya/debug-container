"syntax highlighting based on filenames
filetype on
syntax on

"line numbers
set number

" declare leader key
let mapleader=","

set hidden
set history=100

filetype indent on
set paste
set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
set autoindent
set encoding=utf-8
set noerrorbells
set showcmd
set noswapfile
set nobackup
set splitright
set splitbelow
set autoread 

set backspace=indent,eol,start
set hlsearch

" disable arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

set showmatch

set laststatus=2

" switching between tabs
" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

