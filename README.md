# Debug Container

![Docker Image Size (latest by date)](https://img.shields.io/docker/image-size/serhatcetinkaya/debug) ![Docker Pulls](https://img.shields.io/docker/pulls/serhatcetinkaya/debug) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/serhatcetinkaya/debug-container)

## Description
This container is intended to use when you need to debug something in environments like Docker or Kubernetes. It has many tools installed in it including: `curl`, `tcpdump`, `iftop`, `netcat-openbsd`, `nmap`, `socat`, `strace`, `tcptraceroute`, `openssl`, `python3`, `git`, `mysql-client`, `file`, `jq`, `vim` and `bash`.

The entrypoint is `/bin/bash`.

## Usage

- To debug a running container in its network namespace:

```shell
docker run --rm -it --net container:$PROBLEMATIC_CONTAINER serhatcetinkaya/debug
```

- To debug the host network namespace:

```shell
docker run --rm -it --net host serhatcetinkaya/debug
```

- To run it standalone in Kubernetes:

```shell
kubectl run debug-container --rm -it --image=serhatcetinkaya/debug
```

- To attach a running pod in Kubernetes:

```shell
kubectl debug $PROBLEMATIC_POD --port-forward --agentless --image=serhatcetinkaya/debug
```
