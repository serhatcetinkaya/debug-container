FROM alpine:3.6
MAINTAINER serhatcetinkaya <serhatcetinkaya@pm.me>

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN apk update && apk upgrade
RUN apk add --update ca-certificates openssl && update-ca-certificates
RUN apk add --update wget \
    curl \
    tcpdump \
    iftop \
    netcat-openbsd \
    nmap \
    socat \
    strace \
    tcptraceroute \
    openssl \
    python3 \
    git \
    mysql-client \
    file \
    jq \
    vim \
    bash

RUN rm -rf /var/cache/apk/*

RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 --no-cache-dir install awscli

RUN wget -P /tmp https://github.com/buger/goreplay/releases/download/v1.0.0/gor_1.0.0_x64.tar.gz
RUN tar -zxvf /tmp/gor_1.0.0_x64.tar.gz
RUN mv gor /usr/bin/gor && rm -f /tmp/gor_1.0.0_x64.tar.gz

RUN rm -rf /var/cache/* \
    && rm -rf /root/.cache/*

COPY ./.vimrc /root/.vimrc

RUN echo "alias ll='ls -lh'" >> /root/.bashrc
RUN echo "alias ..='cd ../'" >> /root/.bashrc

ENTRYPOINT ["/bin/bash"]
